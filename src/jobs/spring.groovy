mavenJob('buildSpring') {
    jdk('Java 8')
    scm {
        git('https://gitlab.com/pkiszka/example-spring-boot-app.git')
    }
    goals('clean package')
}
